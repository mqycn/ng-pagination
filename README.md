angular.js的页码组件，基于 directive 实现，样式使用的 bootstrap。

直接在 html代码中加入 以下代码 即可调用：


```
<page-pagination
    page-id="pageId"
    page-record="recordCount"
    page-url-template="urlTemplate">
</page-pagination>
```

**pageId**
当前页面

**recordCount**
记录条数

**urlTemplate**
URL模板

**pageSize**
每页显示的记录条数